package com.epam.model.connection;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

class DBConnectorTest {

    @Test
    void getConnection() {
        Connection connection = DBConnector.getConnection();
        Assert.assertNotNull(connection);
    }

    @Test
    void shutdown() {
        Connection connection = DBConnector.getConnection();
        DBConnector.shutdown();
        try {
            Assert.assertTrue(connection.isClosed());
        } catch (SQLException e) {
            Assert.fail(e.getMessage());
        }
    }
}