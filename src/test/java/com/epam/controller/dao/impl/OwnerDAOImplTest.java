package com.epam.controller.dao.impl;

import com.epam.controller.dao.OwnerDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

class OwnerDAOImplTest {

    private OwnerDAO ownerDAO;

    @BeforeEach
    public void init() {
        ownerDAO = new OwnerDAOImpl();
    }

    @Test
    void deleteWithSale() {
        try {
            Assert.assertSame(1, ownerDAO.deleteWithSale(1, 2));
        } catch (IllegalAccessException | SQLException | InstantiationException e) {
            Assert.fail(e.getMessage());
        }
    }
}