package com.epam.controller.dao.impl;

import com.epam.model.connection.DBConnector;
import com.epam.model.tables.City;
import com.epam.controller.transformer.Transformer;
import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

class CityDAOImplTest {
    private Connection connection;
    private CityDAOImpl cityDAO;
    private Transformer<City> transformer;

    @BeforeEach
    public void initConnection() {
        connection = DBConnector.getConnection();
        cityDAO = new CityDAOImpl();
        transformer = new Transformer<>(City.class);
    }

    @Test
    void findAll() {
        try {
            Set<City> cities = cityDAO.findAll();
            //Assert.assertEquals(10, cities.size());
            System.out.println(cities);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void findByKey() {
        try {
            City city = cityDAO.findByKey(1);
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM city WHERE id = 1");
            rs.next();
            City city1 = (City) transformer.transform(rs);
            Assert.assertEquals(city1, city);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void create() {
        City city = new City(250, "Name", "Country", "Description");
        try {
            Assert.assertEquals(1, cityDAO.create(city));
            cityDAO.delete(city.getId());
        } catch (SQLException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
        try {
            cityDAO.create(new City(1000, "Name", "Country", "Description"));
            cityDAO.delete(1000);
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM city WHERE id = 1000");
            Assert.assertFalse(rs.next());
        } catch (SQLException e) {
            Assert.fail(e.getMessage());
        }
    }

    @After
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void findMostVisitedCity() {
        try {
            City city = cityDAO.findMostVisitedCity();
            Assert.assertNotNull(city);
            Assert.assertEquals("Madrid", city.getName());
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }
}