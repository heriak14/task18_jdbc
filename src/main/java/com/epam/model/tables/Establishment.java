package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.util.Date;
import java.util.Objects;

@Table(name = "establishment")
public class Establishment {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "owner_id")
    private Integer ownerID;
    @Column(name = "foundation_year")
    private Date foundationYear;

    public Establishment() {
    }

    public Establishment(Integer id, String name, String type, Integer ownerID, Date foundationYear) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.ownerID = ownerID;
        this.foundationYear = foundationYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public Date getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(Date foundationYear) {
        this.foundationYear = foundationYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Establishment that = (Establishment) o;
        return id.equals(that.id) &&
                ownerID.equals(that.ownerID) &&
                name.equals(that.name) &&
                type.equals(that.type) &&
                foundationYear.equals(that.foundationYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, ownerID, foundationYear);
    }

    @Override
    public String toString() {
        return "Establishment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", ownerID=" + ownerID +
                ", foundationYear='" + foundationYear + '\'' +
                '}';
    }
}
