package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.util.Arrays;
import java.util.Objects;

@Table(name = "user_login")
public class UserLogin {
    @PrimaryKey
    @Column(name = "id")
    private Integer userID;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private char[] password;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public void setPassword(String password) {
        this.password = password.toCharArray();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLogin userLogin = (UserLogin) o;
        return userID.equals(userLogin.userID) &&
                login.equals(userLogin.login) &&
                Arrays.equals(password, userLogin.password);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(userID, login);
        result = 31 * result + Arrays.hashCode(password);
        return result;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "id=" + userID +
                ", login='" + login + '\'' +
                ", password=" + String.valueOf(password) +
                '}';
    }
}
