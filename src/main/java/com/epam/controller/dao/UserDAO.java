package com.epam.controller.dao;

import com.epam.model.tables.User;

import java.sql.SQLException;
import java.util.Set;

public interface UserDAO extends AbstractDAO<Integer, User> {
    Set<User> getNotActiveUsers() throws SQLException, InstantiationException, IllegalAccessException;
}
