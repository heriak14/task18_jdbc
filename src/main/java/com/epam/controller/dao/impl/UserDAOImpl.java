package com.epam.controller.dao.impl;

import com.epam.controller.dao.UserDAO;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.User;
import com.epam.controller.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class UserDAOImpl implements UserDAO {
    private static final String FIND_ALL_QUERY = "SELECT * FROM user";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM user WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO user VALUES (?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE user SET id = ?, name = ?, country = ?, email = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM user WHERE id = ?";
    private static final String FIND_SPECIFIC = "SELECT * FROM user WHERE id NOT IN (SELECT user_id FROM feedback)";
    private Transformer<User> transformer;

    public UserDAOImpl() {
        transformer = new Transformer<>(User.class);
    }

    @Override
    public Set<User> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        return findUsers(FIND_ALL_QUERY);
    }

    @Override
    public User findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return (User) transformer.transform(resultSet);
            } else {
                return new User();
            }
        }
    }

    @Override
    public int create(User entity) throws SQLException {
        return prepareStatement(CREATE_QUERY, entity).executeUpdate();
    }

    @Override
    public int update(User entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(5, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, User entity) throws SQLException {
        try(PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getCountry());
            preparedStatement.setString(4, entity.getEmail());
            return preparedStatement;
        }
    }

    @Override
    public Set<User> getNotActiveUsers() throws SQLException, InstantiationException, IllegalAccessException {
        return findUsers(FIND_SPECIFIC);
    }

    private Set<User> findUsers(String query) throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            Set<User> users = new HashSet<>();
            while (resultSet.next()) {
                users.add((User) transformer.transform(resultSet));
            }
            return users;
        }
    }
}
