package com.epam.controller.dao.impl;

import com.epam.model.connection.DBConnector;
import com.epam.controller.dao.AbstractDAO;
import com.epam.model.tables.Feedback;
import com.epam.controller.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class FeedbackDAO implements AbstractDAO<Integer, Feedback> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM feedback";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM feedback WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO feedback VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE city SET id = ?, user_id = ?, city_establishment_id = ?, "
            + "feedback = ?, date = ?, rating = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM feedback WHERE id = ?";
    private Transformer<Feedback> transformer;

    public FeedbackDAO() {
        transformer = new Transformer<>(Feedback.class);
    }

    @Override
    public Set<Feedback> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<Feedback> feedbacks = new HashSet<>();
            while (resultSet.next()) {
                feedbacks.add((Feedback) transformer.transform(resultSet));
            }
            return feedbacks;
        }
    }

    @Override
    public Feedback findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet resultSet = statement.executeQuery(FIND_BY_KEY_QUERY);
            if (resultSet.next()) {
                return (Feedback) transformer.transform(resultSet);
            } else {
                return new Feedback();
            }
        }
    }

    @Override
    public int create(Feedback entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(CREATE_QUERY, entity)) {
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Feedback entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(7, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, Feedback entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, String.valueOf(entity.getUserID()));
            preparedStatement.setString(3, String.valueOf(entity.getCityEstablishmentID()));
            preparedStatement.setString(4, entity.getFeedback());
            preparedStatement.setString(5, String.valueOf(entity.getDate()));
            preparedStatement.setString(6, String.valueOf(entity.getRating()));
            return preparedStatement;
        }
    }
}
