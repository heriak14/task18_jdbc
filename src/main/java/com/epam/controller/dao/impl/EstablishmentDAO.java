package com.epam.controller.dao.impl;

import com.epam.model.connection.DBConnector;
import com.epam.controller.dao.AbstractDAO;
import com.epam.model.tables.Establishment;
import com.epam.controller.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class EstablishmentDAO implements AbstractDAO<Integer, Establishment> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM establishment";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM establishment WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO establishment VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE establishment SET id = ?, name = ?, type = ?, owner_id = ?, "
            + "foundation_year = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM establishment WHERE id = ?";
    private Transformer<Establishment> transformer;

    public EstablishmentDAO() {
        transformer = new Transformer<>(Establishment.class);
    }

    @Override
    public Set<Establishment> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<Establishment> establishments = new HashSet<>();
            while (resultSet.next()) {
                establishments.add((Establishment) transformer.transform(resultSet));
            }
            return establishments;
        }
    }

    @Override
    public Establishment findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet resultSet = statement.executeQuery();
            return (Establishment) transformer.transform(resultSet);
        }
    }

    @Override
    public int create(Establishment entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(CREATE_QUERY, entity)) {
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Establishment entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(6, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, Establishment entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getType());
            preparedStatement.setString(4, String.valueOf(entity.getOwnerID()));
            preparedStatement.setString(5, String.valueOf(entity.getFoundationYear()));
            return preparedStatement;
        }
    }
}
