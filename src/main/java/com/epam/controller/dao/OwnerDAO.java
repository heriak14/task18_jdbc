package com.epam.controller.dao;

import com.epam.model.tables.Owner;

import java.sql.SQLException;

public interface OwnerDAO extends AbstractDAO<Integer, Owner> {
    int deleteWithSale(int sellerId, int customerId) throws IllegalAccessException, SQLException, InstantiationException;
}
