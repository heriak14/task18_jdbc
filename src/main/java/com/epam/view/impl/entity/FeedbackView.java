package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.FeedbackDAO;
import com.epam.model.tables.Feedback;
import com.epam.view.impl.EntityView;

import java.sql.Date;
import java.sql.SQLException;

public class FeedbackView extends EntityView {
    private FeedbackDAO feedbackDAO;

    public FeedbackView() {
        feedbackDAO = new FeedbackDAO();
    }

    @Override
    protected void findAll() {
        try {
            feedbackDAO.findAll().forEach(f -> LOGGER.info(f + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("feedback id");
        try {
            LOGGER.info(feedbackDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        Feedback feedback = inputFeedback();
        try {
            feedbackDAO.create(feedback);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        Feedback feedback = inputFeedback();
        try {
            feedbackDAO.update(feedback);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Feedback inputFeedback() {
        Feedback feedback = new Feedback();
        Integer id = getIntInput("id");
        feedback.setId(id);
        Integer userId = getIntInput("user id");
        feedback.setUserID(userId);
        Integer cityEstId = getIntInput("city establishment id");
        feedback.setCityEstablishmentID(cityEstId);
        LOGGER.info("Enter feedback: ");
        feedback.setFeedback(SCANNER.nextLine());
        LOGGER.info("Enter date: ");
        feedback.setDate(Date.valueOf(SCANNER.nextLine()));
        Double rating = getRatingInput();
        feedback.setRating(rating);
        return feedback;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(feedbackDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }

    private Double getRatingInput() {
        LOGGER.info("Enter rating: ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+.?\\d*")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0.0;
        }
        return Double.parseDouble(id);
    }
}
