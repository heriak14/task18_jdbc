package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.UserLoginDAO;
import com.epam.model.tables.UserLogin;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;

public class UserLoginView extends EntityView {
    private UserLoginDAO userLoginDAO;

    public UserLoginView() {
        userLoginDAO = new UserLoginDAO();
    }

    @Override
    protected void findAll() {
        try {
            userLoginDAO.findAll().forEach(u -> LOGGER.info(u + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput();
        try {
            LOGGER.info(userLoginDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        UserLogin user = inputUserLogin();
        try {
            userLoginDAO.create(user);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        UserLogin userLogin = inputUserLogin();
        try {
            userLoginDAO.update(userLogin);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private UserLogin inputUserLogin() {
        UserLogin userLogin = new UserLogin();
        Integer id = getIntInput();
        userLogin.setUserID(id);
        LOGGER.info("Enter user login: ");
        userLogin.setLogin(SCANNER.nextLine());
        LOGGER.info("Enter password: ");
        userLogin.setPassword(SCANNER.nextLine());
        return userLogin;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput();
            LOGGER.info(userLoginDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput() {
        LOGGER.info("Enter id: ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }
}
