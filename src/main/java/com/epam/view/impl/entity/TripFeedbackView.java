package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.TripFeedbackDAO;
import com.epam.model.tables.TripFeedback;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;

public class TripFeedbackView extends EntityView {
    private TripFeedbackDAO tripFeedbackDAO;

    public TripFeedbackView() {
        tripFeedbackDAO = new TripFeedbackDAO();
    }

    @Override
    protected void findAll() {
        try {
            tripFeedbackDAO.findAll().forEach(t -> LOGGER.info(t + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("id");
        try {
            LOGGER.info(tripFeedbackDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        TripFeedback tripFeedback = inputTripFeedback();
        try {
            tripFeedbackDAO.create(tripFeedback);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        TripFeedback tripFeedback = inputTripFeedback();
        try {
            tripFeedbackDAO.update(tripFeedback);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private TripFeedback inputTripFeedback() {
        TripFeedback tripFeedback = new TripFeedback();
        Integer id = getIntInput("id");
        tripFeedback.setId(id);
        Integer tripId = getIntInput("planed trip id");
        tripFeedback.setPlanedTripID(tripId);
        Integer feedbackId = getIntInput("feedback id");
        tripFeedback.setFeedbackID(feedbackId);
        return tripFeedback;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(tripFeedbackDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }
}
