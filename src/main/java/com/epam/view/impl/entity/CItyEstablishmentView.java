package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.CityEstablishmentDAO;
import com.epam.model.tables.CityEstablishment;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;

public class CItyEstablishmentView extends EntityView {
    private CityEstablishmentDAO cityEstablishmentDAO;

    public CItyEstablishmentView() {
        cityEstablishmentDAO = new CityEstablishmentDAO();
    }

    @Override
    protected void findAll() {
        try {
            cityEstablishmentDAO.findAll().forEach(c -> LOGGER.info(c + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("id");
        try {
            LOGGER.info(cityEstablishmentDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        CityEstablishment establishment = inputCityEstablishment();
        try {
            cityEstablishmentDAO.create(establishment);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        CityEstablishment establishment = inputCityEstablishment();
        try {
            cityEstablishmentDAO.update(establishment);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private CityEstablishment inputCityEstablishment() {
        CityEstablishment establishment = new CityEstablishment();
        Integer id = getIntInput("id");
        establishment.setId(id);
        LOGGER.info("Enter city id: ");
        Integer cityId = getIntInput("city id");
        establishment.setCityID(cityId);
        LOGGER.info("Enter establishment id: ");
        Integer estId = getIntInput("establishment id");
        establishment.setEstablishmentID(estId);
        LOGGER.info("Enter street: ");
        establishment.setStreet(SCANNER.nextLine());
        LOGGER.info("Enter building: ");
        establishment.setBuilding(SCANNER.nextLine());
        return establishment;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(cityEstablishmentDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }
}
