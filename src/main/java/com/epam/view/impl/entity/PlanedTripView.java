package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.PlanedTripDAO;
import com.epam.model.tables.PlanedTrip;
import com.epam.view.impl.EntityView;

import java.sql.Date;
import java.sql.SQLException;

public class PlanedTripView extends EntityView {
    private PlanedTripDAO planedTripDAO;

    public PlanedTripView() {
        planedTripDAO = new PlanedTripDAO();
    }

    @Override
    protected void findAll() {
        try {
            planedTripDAO.findAll().forEach(t -> LOGGER.info(t + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("id");
        try {
            LOGGER.info(planedTripDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        PlanedTrip planedTrip = inputPlanedTrip();
        try {
            planedTripDAO.create(planedTrip);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        PlanedTrip planedTrip = inputPlanedTrip();
        try {
            planedTripDAO.update(planedTrip);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private PlanedTrip inputPlanedTrip() {
        PlanedTrip planedTrip = new PlanedTrip();
        Integer id = getIntInput("id");
        planedTrip.setId(id);
        LOGGER.info("Enter name: ");
        planedTrip.setName(SCANNER.nextLine());
        Integer userId = getIntInput("user id");
        planedTrip.setUserID(userId);
        Integer cityId = getIntInput("city id");
        planedTrip.setCityID(cityId);
        LOGGER.info("Enter date: ");
        planedTrip.setDate(Date.valueOf(SCANNER.nextLine()));
        return planedTrip;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(planedTripDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }
}
