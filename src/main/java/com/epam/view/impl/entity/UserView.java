package com.epam.view.impl.entity;

import com.epam.controller.dao.UserDAO;
import com.epam.controller.dao.impl.UserDAOImpl;
import com.epam.model.tables.User;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserView extends EntityView {
    private UserDAO userDAO;

    public UserView() {
        userDAO = new UserDAOImpl();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> userMethods = new LinkedHashMap<>();
        userMethods.put("1", this::findAll);
        userMethods.put("2", this::findByKey);
        userMethods.put("3", this::create);
        userMethods.put("4", this::update);
        userMethods.put("5", this::delete);
        userMethods.put("6", this::showNotActiveUsers);
        userMethods.put("Q", this::quit);
        return userMethods;
    }

    @Override
    protected Map<String, String> setMenu() {
        Map<String, String> menu = new Menu("properties/crud_menu").getMenu();
        menu.put("6", "Show not active users");
        return menu;
    }

    @Override
    protected void findAll() {
        try {
            userDAO.findAll().forEach(u -> LOGGER.info(u + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput();
        try {
            LOGGER.info(userDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        User user = inputUser();
        try {
            userDAO.create(user);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        User user = inputUser();
        try {
            userDAO.update(user);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private User inputUser() {
        User user = new User();
        Integer id = getIntInput();
        user.setId(id);
        LOGGER.info("Enter user name: ");
        user.setName(SCANNER.nextLine());
        LOGGER.info("Enter country: ");
        user.setCountry(SCANNER.nextLine());
        LOGGER.info("Enter email: ");
        user.setEmail(SCANNER.nextLine());
        return user;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput();
            LOGGER.info(userDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput() {
        LOGGER.info("Enter id: ");
        String id = SCANNER.nextLine();
        if (id.matches("\\D")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }

    private void showNotActiveUsers() {
        try {
            userDAO.getNotActiveUsers().forEach(u -> LOGGER.info(u + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
