package com.epam.view.impl;

import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;
import com.epam.view.impl.entity.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class TableView implements View {
    private Map<String, String> tableMenu;
    private Map<String, Printable> tableMethods;
    private EntityView view;

    TableView() {
        tableMenu = new Menu("properties/tables_menu").getMenu();
        tableMethods = setMethods();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> tableMethods = new LinkedHashMap<>();
        tableMethods.put("1", this::chooseUserTable);
        tableMethods.put("2", this::chooseUserLoginTable);
        tableMethods.put("3", this::chooseCityTable);
        tableMethods.put("4", this::chooseEstablishmentTable);
        tableMethods.put("5", this::chooseCityEstablishmentTable);
        tableMethods.put("6", this::chooseOwnerTable);
        tableMethods.put("7", this::chooseFeedbackTable);
        tableMethods.put("8", this::choosePlanedTripTable);
        tableMethods.put("9", this::chooseTripFeedbackTable);
        tableMethods.put("Q", this::quit);
        return tableMethods;
    }

    private void chooseUserTable() {
        view = new UserView();
        view.show();
    }

    private void chooseUserLoginTable() {
        view = new UserLoginView();
        view.show();
    }

    private void chooseCityTable() {
        view = new CityView();
        view.show();
    }

    private void chooseEstablishmentTable() {
        view = new EstablishmentView();
        view.show();
    }

    private void chooseCityEstablishmentTable() {
        view = new CItyEstablishmentView();
        view.show();
    }

    private void chooseOwnerTable() {
        view = new OwnerView();
        view.show();
    }

    private void chooseFeedbackTable() {
        view = new FeedbackView();
        view.show();
    }

    private void choosePlanedTripTable() {
        view = new PlanedTripView();
        view.show();
    }

    private void chooseTripFeedbackTable() {
        view = new TripFeedbackView();
        view.show();
    }

    private void quit() {
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "TABLE MENU______________________\n");
        for (Map.Entry entry : tableMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose one table: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if (tableMethods.containsKey(option)) {
                tableMethods.get(option).print();
            } else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
