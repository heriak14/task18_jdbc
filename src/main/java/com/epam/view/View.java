package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Scanner;

public interface View {
    Logger LOGGER = LogManager.getLogger();
    Scanner SCANNER = new Scanner(System.in);
    Map<String, Printable> setMethods();
    void printMenu();
    void show();
}
